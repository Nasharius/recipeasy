//
//  AlertsHelper.swift
//  Recipeasy
//
//  Created by James Hoffman on 2018-10-02.
//  Copyright © 2018 James Hoffman. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func alert(title: String, message: String? = nil, actions: [UIAlertAction]) {
        let alertController = UIAlertController(title: title, message:
            message, preferredStyle: UIAlertController.Style.alert)
        
        for action in actions {
            alertController.addAction(action)
        }
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func okAlert(title: String, message: String? = nil, handler: ((UIAlertAction) -> Void)? = nil) {
        alert(title: title, message: message, actions: [UIAlertAction(title: "Ok", style: UIAlertAction.Style.default,handler: handler)])
    }
}
