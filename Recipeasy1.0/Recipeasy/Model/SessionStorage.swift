//
//  SessionStorage.swift
//  Recipeasy
//
//  Created by Owner: SessionIntensive on 19-01-23.
//  Copyright © 2019 James Hoffman. All rights reserved.
//

import Foundation

class SessionStorage {
    
    //
    // UserDefault Username
    //
    private static let USERDEFAULTS_PASSPHRASE_KEY = "demo.userdefault.username"
    static var userDefaultsPassphrase : String {
        get {
            return UserDefaults.standard.string(forKey: USERDEFAULTS_PASSPHRASE_KEY) ?? ""
        }
        
        set {
            UserDefaults.standard.set(newValue, forKey: USERDEFAULTS_PASSPHRASE_KEY)
        }
    }
    
    //
    // Password
    //
    private static let KEYCHAIN_PASSPHRASE_KEY = "demo.keychain.username"
    static var keychainPassphrase: String {
        get {
            return KeychainWrapper.standard.string(forKey: KEYCHAIN_PASSPHRASE_KEY) ?? ""
        }
        
        set {
            KeychainWrapper.standard.set(newValue, forKey: KEYCHAIN_PASSPHRASE_KEY)
        }
    }
}
