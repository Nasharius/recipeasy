//
//  RecipeCategory.swift
//  Recipeasy
//
//  Created by James Hoffman on 2018-09-11.
//  Copyright © 2018 James Hoffman. All rights reserved.
//

import Foundation

enum RecipeCategory: Int {
    case None = -1
    case Breakfast = 0
    case Lunch = 1
    case Dinner = 2
}
