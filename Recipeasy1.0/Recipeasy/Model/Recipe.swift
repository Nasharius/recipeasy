//
//  Recipe.swift
//  Recipeasy
//
//  Created by James Hoffman on 2018-09-11.
//  Copyright © 2018 James Hoffman. All rights reserved.
//

import Foundation

class Recipe {
    public var id: Int
    public var category: RecipeCategory
    public var name: String
    public var duration: TimeInterval
    public var description: String
    public var image: String
    
    public var durationStringRepresentation: String {
        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = .abbreviated
        formatter.allowedUnits = [.hour, .minute]
        
        return formatter.string(from: self.duration)!
    }
    
    public var dictionaryRepresention: NSDictionary {
        let dictionary = NSMutableDictionary()
        
        dictionary.setValue(id, forKey: "id")
        dictionary.setValue(category.rawValue, forKey: "category")
        dictionary.setValue(name, forKey: "name")
        dictionary.setValue(duration, forKey: "duration")
        dictionary.setValue(description, forKey: "description")
        dictionary.setValue(image, forKey: "image")
        
        return dictionary
    }
    
    init() {
        id = -1
        category = .None
        name = ""
        duration = 0
        description = ""
        image = ""
    }
    
    init?(recipeJson: NSDictionary) {
        if let image = recipeJson["image"] as? String{
            self.image = image
        }else{
            self.image = ""
        }
        
        if let id = recipeJson["id"] as? Int,
        let category = recipeJson["category"] as? Int,
        let enumCategory = RecipeCategory(rawValue: category),
        let name = recipeJson["name"] as? String,
        let duration = recipeJson["duration"] as? TimeInterval,
        let description = recipeJson["description"] as? String{
            self.id = id
            self.category = enumCategory
            self.name = name
            self.duration = duration
            self.description = description
        } else {
            return nil
        }
    }
}
