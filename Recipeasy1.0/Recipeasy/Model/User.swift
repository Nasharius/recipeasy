//
//  User.swift
//  Recipeasy
//
//  Created by James Hoffman on 2018-09-15.
//  Copyright © 2018 James Hoffman. All rights reserved.
//

import Foundation

class User {
    private(set) var id: Int
    private(set) var username: String
    
    var recipes: [Recipe] = []
    
    init(id: Int, username: String) {
        self.id = id
        self.username = username
    }
}
