//
//  RecipesTableViewController.swift
//  Recipeasy
//
//  Created by James Hoffman on 2018-09-11.
//  Copyright © 2018 James Hoffman. All rights reserved.
//

import UIKit

class RecipesTableViewController: UITableViewController, UISearchResultsUpdating {
    var currentUser: User!
    let searchController = UISearchController(searchResultsController: nil)
    var tableData: [Recipe] = []
    var idx: Int = 0
    
    override func viewDidLoad() {
        self.navigationItem.title = String.localizedStringWithFormat(NSLocalizedString("Recipe's owner", comment: ""), currentUser.username)
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = NSLocalizedString("Search recipe", comment: "")
        navigationItem.searchController = searchController // Will show search in navbar when scrolling up
        definesPresentationContext = true
        // Fix navigation stack by remove sign up form if necessary
        let signUpControllerId = self.navigationController?.viewControllers.index(where: { (viewController) -> Bool in
            return viewController is SignupViewController
        })
        
        if let removeId = signUpControllerId {
            self.navigationController?.viewControllers.remove(at: removeId)
        }
        
        // Add log out button to navbar
        self.navigationItem.hidesBackButton = true
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Logout", comment: ""), style: .plain, target: self, action: #selector(onLogOutBarButtonTapped))
        
        // Fetch recipes
        MarthaRequest.getUserRecipes(userId: currentUser.id) { (recipes) in
            
            if let fetchedRecipes = recipes {
                self.currentUser.recipes = fetchedRecipes
                self.tableData = fetchedRecipes
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
        
    }
    
    @objc func onLogOutBarButtonTapped() {
        self.navigationController?.popViewController(animated: true)
        /*UserDefaults.standard.set("", forKey: "username")
        let _: Bool = KeychainWrapper.standard.set("", forKey: "myKey")*/
        SessionStorage.userDefaultsPassphrase = ""
        SessionStorage.keychainPassphrase = ""
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "recipes.recipe-edit") {
            if let recipeViewController = segue.destination as? RecipeViewController,
               let recipeToEdit = sender as? Recipe {
                recipeViewController.recipe = recipeToEdit
                recipeViewController.userId = currentUser.id
            }
        } else if (segue.identifier == "recipes.recipe-add") {
            if let recipeViewController = segue.destination as? RecipeViewController {
                recipeViewController.userId = currentUser.id
            }
        }
    }
    
    @IBAction func unwindAddToList(segue: UIStoryboardSegue) {
        if let newRecipe = (segue.source as? RecipeViewController)?.recipe {
            currentUser.recipes.append(newRecipe)
        }
        
        tableView.reloadData()
    }
    
    @IBAction func unwindDeleteToList(segue: UIStoryboardSegue) {
        if let recipeToDelete = (segue.source as? RecipeViewController)?.recipe {
            deleteRecipe(recipeToDelete)
        }
        
        tableView.reloadData()
    }
    
    private func deleteRecipe(_ recipeToDelete: Recipe) {
        currentUser.recipes.removeAll { (recipe) -> Bool in
            recipe.id == recipeToDelete.id
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "recipes.recipe", for: indexPath)
        
        let recipe = tableData[indexPath.row]
        cell.textLabel?.text = recipe.name
        cell.detailTextLabel?.text = recipe.durationStringRepresentation

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let recipe = tableData[indexPath.row]
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        performSegue(withIdentifier: "recipes.recipe-edit", sender: recipe)
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .normal, title: "Delete") { (action, indexPath) in
            let recipeToDelete = self.tableData[indexPath.row]
            
            MarthaRequest.deleteRecipe(recipeToDelete, completion: { success in
                DispatchQueue.main.async {
                    if (success) {
                        self.deleteRecipe(recipeToDelete)
                        
                        tableView.reloadData()
                    } else {
                        self.okAlert(title: NSLocalizedString("Error", comment: ""), message: NSLocalizedString("Could not delete recipe", comment: ""))
                    }
                }
            })
        }
        deleteAction.backgroundColor = UIColor.red
        
        return [deleteAction]
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        
        let lowercasedCriteria = (searchController.searchBar.text ?? "").lowercased()
        if (lowercasedCriteria.isEmpty) {
            idx+=1
            MarthaRequest.getUserRecipes(userId: currentUser.id) { (recipes) in
                
                if let fetchedRecipes = recipes {
                    self.tableData = fetchedRecipes
                    
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
            }
        } else {
            idx += 1
            MarthaRequest.fullTextFetch(userId: currentUser.id, search: lowercasedCriteria,token: idx) { (recipes,token) in
                
                if let fetchedRecipes = recipes {
                    if(token == self.idx){
                        self.currentUser.recipes = fetchedRecipes
                        self.tableData = fetchedRecipes
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                        }
                    }
                    
                }
            }
        }
        self.tableView.reloadData()
    }
}
