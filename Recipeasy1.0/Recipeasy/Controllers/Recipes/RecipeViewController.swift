//
//  ViewController.swift
//  Recipeasy
//
//  Created by James Hoffman on 2018-09-11.
//  Copyright © 2018 James Hoffman. All rights reserved.
//

import UIKit

class RecipeViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @IBOutlet weak var categorySegmentedControl: UISegmentedControl!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var durationPicker: UIDatePicker!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var recipePicture: UIImageView!
    
    var recipe: Recipe?
    var userId: Int!
    private var isEdit: Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        
        if let currentRecipe = self.recipe {
            isEdit = true
            
            self.navigationItem.title = NSLocalizedString("Edit recipe", comment: "")
            categorySegmentedControl.selectedSegmentIndex = currentRecipe.category.rawValue
            nameTextField.text = currentRecipe.name
            durationPicker.countDownDuration = currentRecipe.duration
            descriptionTextView.text = currentRecipe.description
            
            recipePicture.image = convertBase64ToImage(string: currentRecipe.image)
        } else {
            isEdit = false
            
            self.navigationItem.title = NSLocalizedString("Add recipe", comment: "")
            self.recipe = Recipe();
            updateSelectedImage(image: UIImage(named: "placeholder")!)
        }
        
    }
   
    @IBAction func openLibraryButtonTapped(_ sender: Any) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.sourceType = .photoLibrary
        imagePickerController.delegate = self
        
        present(imagePickerController, animated: true)
    }
    
    @IBAction func onSaveButtonTapped() {
        let category = RecipeCategory(rawValue: categorySegmentedControl.selectedSegmentIndex)!
        let name = nameTextField.text ?? ""
        let duration = durationPicker.countDownDuration
        let description = descriptionTextView.text ?? ""
        
        
        
        if (category == .None || name.isEmpty) {
            okAlert(title: NSLocalizedString("Error", comment: ""), message: NSLocalizedString("Category/name", comment: ""))
        } else {
            
            self.recipe!.category = category
            self.recipe!.name = name
            self.recipe!.duration = duration
            self.recipe!.description = description
            
            
            if (isEdit) {
                MarthaRequest.updateRecipe(recipe!) { (success) in
                    if (success) {
                        DispatchQueue.main.async {
                            self.recipe = nil
                            self.performSegue(withIdentifier: "recipe.unwind-to-list", sender: self)
                        }
                    }
                }
            } else {
                MarthaRequest.addRecipe(recipe!, forUserId: userId) { (newRecipe) in
                    if(newRecipe != nil) {
                        DispatchQueue.main.async {
                            self.performSegue(withIdentifier: "recipe.unwind-to-list", sender: self)
                        }
                    }
                }
            }
        }
    }
    func updateSelectedImage(image: UIImage) {
        let newWidth = Int(500)
        let newHeight = Int(500)
 
        if let resizedImage = resize(image: image, width: newWidth, height: newHeight),
            let b64img = convertImageToBase64(image: resizedImage){ // image -> b64
            recipePicture.image = resizedImage
            self.recipe!.image = b64img
            //print(b64Image) // voir la console pour la string b64
            
        }
    }
    func convertBase64ToImage(string: String)-> UIImage? {
        if let data = Data(base64Encoded: string, options: .ignoreUnknownCharacters) {
            return UIImage(data: data)
        }
        
        return nil
    }
    func convertImageToBase64(image: UIImage)-> String? {
        if let data: Data = image.pngData() {
            return data.base64EncodedString()
        }
        
        return nil
    }
    func resize(image: UIImage, width: Int, height: Int)-> UIImage? {
        UIGraphicsBeginImageContextWithOptions(CGSize(width: width, height: height), true, 1.0)
        image.draw(in: CGRect(x: 0, y: 0, width: width, height: height))
        let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return resizedImage
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        picker.dismiss(animated: true) // On recoit en param le view controller qui a ete utilise pour prendre la photo, on doit le masquer une fois que l'image est recuperee
        
        // Plusieurs metadonnees sont disponibles
        //print(info)
        
        // Pour recuperer l'image
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            updateSelectedImage(image: image)
        } else {
            print(NSLocalizedString("No image", comment: ""))
        }
    }
    @IBAction func onDeleteButtonTapped() {
        var actions: [UIAlertAction] = []
        actions.append(UIAlertAction(title: NSLocalizedString("No", comment: ""), style: UIAlertAction.Style.default))
        actions.append(UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: UIAlertAction.Style.destructive, handler: deleteRecipe))
        
        alert(title: NSLocalizedString("Delete", comment: ""), message: nil, actions: actions)
    }
    
    private func deleteRecipe(action: UIAlertAction) {
        if (self.isEdit) {
            MarthaRequest.deleteRecipe(self.recipe!, completion: { success in
                DispatchQueue.main.async {
                    if (success) {
                        self.performSegue(withIdentifier: "recipe.unwind-delete-to-list", sender: self)
                    } else {
                        self.okAlert(title: NSLocalizedString("Error", comment: ""), message: NSLocalizedString("Could not delete recipe", comment: ""))
                    }
                }
            })
        } else {
            self.recipe = nil
            self.performSegue(withIdentifier: "recipe.unwind-delete-to-list", sender: self)
        }
    }
}

