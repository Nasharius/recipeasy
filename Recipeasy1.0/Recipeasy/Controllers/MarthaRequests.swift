//
//  MarthaRequests.swift
//  Recipeasy
//
//  Created by James Hoffman on 2018-09-16.
//  Copyright © 2018 James Hoffman. All rights reserved.
//

import Foundation

class MarthaRequest {
    private static let auth = "bWFyY2FuZHJlOjExNTQ0OTg=" // demo
    // private static let auth = "cmVjaXBlYXN5OnJlY2lwZTEyMw==" // recipeasy
    
    private static func request(request: String, params: Data? = nil, completion: @escaping ((Data?, URLResponse?, Error?) -> Void)) {
        let url:URL = URL(string: "http://b7b.jh.shawinigan.info/queries/\(request)/execute")!
        let session = URLSession.shared
        
        let request = NSMutableURLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue(auth, forHTTPHeaderField: "auth")
        request.httpBody = params
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: completion)
        task.resume()
    }
    
    public static func login(username: String, password: String, completion: @escaping ((User?)->Void)) {
        let paramString = "{\"username\":\"\(username)\", \"password\":\"\(password)\"}"
        let params = paramString.data(using: String.Encoding.utf8)
        
        self.request(request: "select-user-login", params: params)
        { (requestData, requestResponse, requestError) in
            
            if let httpError = requestError {
                print(httpError)
                completion(nil)
            } else if let data = requestData {
                do {
                    let jsonResponse = try JSONSerialization.jsonObject(with: data)
                    
                    if let json = jsonResponse as? NSDictionary,
                        let success = json["success"] as? Bool,
                        let user = (json["data"] as? NSArray)?.firstObject as? NSDictionary,
                        let id = user["id"] as? Int,
                        let name = user["username"] as? String
                    {
                        if (success) {
                            completion(User(id: id, username: name))
                        } else {
                            completion(nil)
                        }
                    } else {
                        completion(nil)
                    }
                }
                catch let parsingError
                {
                    //JSON Parsing error
                    print(parsingError)
                    completion(nil)
                }
            } else {
                completion(nil)
            }
        }
    }
    
    public static func signUp(username: String, password: String, completion: @escaping ((User?)->Void)) {
        let paramString = "{\"username\":\"\(username)\", \"password\":\"\(password)\"}"
        let params = paramString.data(using: String.Encoding.utf8)
        
        self.request(request: "insert-user-signup", params: params) {(requestData, requestResponse, requestError) in
            
            if let httpError = requestError {
                print(httpError)
                completion(nil)
            } else if let data = requestData {
                do {
                    let jsonResponse = try JSONSerialization.jsonObject(with: data)
                    
                    if let json = jsonResponse as? NSDictionary,
                        let success = json["success"] as? Bool,
                        let id = json["lastInsertId"] as? Int
                    {
                        if (success) {
                            completion(User(id: id, username: username))
                        } else {
                            completion(nil)
                        }
                    } else {
                        completion(nil)
                    }
                }
                catch let parsingError
                {
                    //JSON Parsing error
                    print(parsingError)
                    completion(nil)
                }
            } else {
                completion(nil)
            }
        }
    }
    
    public static func fullTextFetch(userId: Int,search: String,token: Int, completion: @escaping (([Recipe]?,Int)->Void)) {
        let paramString = "{\"id\":\"\(userId)\", \"search\":\"\(search)\"}"
        let params = paramString.data(using: String.Encoding.utf8)
        
        self.request(request: "full-text-search-recipe", params: params) {(requestData, requestResponse, requestError) in
            
            if let httpError = requestError {
                print(httpError)
                completion(nil,-1)
            } else if let data = requestData {
                do {
                    let jsonResponse = try JSONSerialization.jsonObject(with: data)
                    
                    if let json = jsonResponse as? NSDictionary,
                        let success = json["success"] as? Bool,
                        let jsonRecipes = json["data"] as? NSArray
                    {
                        if (success) {
                            var recipes: [Recipe] = []
                            for jsonRecipe in jsonRecipes {
                                if let parsedJsonRecipe = jsonRecipe as? NSDictionary,
                                    let recipe = Recipe(recipeJson: parsedJsonRecipe) {
                                    recipes.append(recipe)
                                }
                            }
                            
                            completion(recipes,token)
                        } else {
                            completion(nil,-1)
                        }
                    } else {
                        completion(nil,-1)
                    }
                }
                catch let parsingError
                {
                    //JSON Parsing error
                    print(parsingError)
                    completion(nil,-1)
                }
            } else {
                completion(nil,-1)
            }
        }
    }
    
    public static func getUserRecipes(userId: Int, completion: @escaping (([Recipe]?)->Void)) {
        let paramString = "{\"id\":\"\(userId)\"}"
        let params = paramString.data(using: String.Encoding.utf8)
        
        self.request(request: "select-recipes", params: params) {(requestData, requestResponse, requestError) in
            
            if let httpError = requestError {
                print(httpError)
                completion(nil)
            } else if let data = requestData {
                do {
                    let jsonResponse = try JSONSerialization.jsonObject(with: data)
                    
                    if let json = jsonResponse as? NSDictionary,
                        let success = json["success"] as? Bool,
                        let jsonRecipes = json["data"] as? NSArray
                    {
                        if (success) {
                            var recipes: [Recipe] = []
                            for jsonRecipe in jsonRecipes {
                                if let parsedJsonRecipe = jsonRecipe as? NSDictionary,
                                    let recipe = Recipe(recipeJson: parsedJsonRecipe) {
                                    recipes.append(recipe)
                                }
                            }
                            
                            completion(recipes)
                        } else {
                            completion(nil)
                        }
                    } else {
                        completion(nil)
                    }
                }
                catch let parsingError
                {
                    //JSON Parsing error
                    print(parsingError)
                    completion(nil)
                }
            } else {
                completion(nil)
            }
        }
    }
    
    public static func addRecipe(_ recipe: Recipe, forUserId userId: Int, completion: @escaping ((Recipe?)->Void)) {
        let paramsDictionary = NSMutableDictionary(dictionary: recipe.dictionaryRepresention)
        paramsDictionary.setValue(userId, forKey: "user_id")
        let params = try! JSONSerialization.data(withJSONObject: paramsDictionary)
        
        self.request(request: "insert-recipe", params: params) {(requestData, requestResponse, requestError) in
            
            if let httpError = requestError {
                print(httpError)
                completion(nil)
            } else if let data = requestData {
                do {
                    let jsonResponse = try JSONSerialization.jsonObject(with: data)
                    
                    if let json = jsonResponse as? NSDictionary,
                        let success = json["success"] as? Bool,
                        let id = json["lastInsertId"] as? Int
                    {
                        if (success) {
                            recipe.id = id
                            completion(recipe)
                        } else {
                            completion(nil)
                        }
                    } else {
                        completion(nil)
                    }
                }
                catch let parsingError
                {
                    //JSON Parsing error
                    print(parsingError)
                    completion(nil)
                }
            } else {
                completion(nil)
            }
        }
    }
    
    public static func updateRecipe(_ recipe: Recipe, completion: @escaping ((Bool)->Void)) {
        let paramsDictionary = recipe.dictionaryRepresention
        let params = try! JSONSerialization.data(withJSONObject: paramsDictionary)
        
        self.request(request: "update-recipe", params: params) {(requestData, requestResponse, requestError) in
            
            if let httpError = requestError {
                print(httpError)
                completion(false)
            } else if let data = requestData {
                do {
                    let jsonResponse = try JSONSerialization.jsonObject(with: data)
                    
                    if let json = jsonResponse as? NSDictionary,
                        let success = json["success"] as? Bool
                    {
                        if (success) {
                            completion(true)
                        } else {
                            completion(false)
                        }
                    } else {
                        completion(false)
                    }
                }
                catch let parsingError
                {
                    //JSON Parsing error
                    print(parsingError)
                    completion(false)
                }
            } else {
                completion(false)
            }
        }
    }
    
    public static func deleteRecipe(_ recipe: Recipe, completion: @escaping ((Bool)->Void)) {
        let paramsDictionary = recipe.dictionaryRepresention
        let params = try! JSONSerialization.data(withJSONObject: paramsDictionary)
        
        self.request(request: "delete-recipe", params: params) {(requestData, requestResponse, requestError) in
            
            if let httpError = requestError {
                print(httpError)
                completion(false)
            } else if let data = requestData {
                do {
                    let jsonResponse = try JSONSerialization.jsonObject(with: data)
                    
                    if let json = jsonResponse as? NSDictionary,
                        let success = json["success"] as? Bool
                    {
                        if (success) {
                            completion(true)
                        } else {
                            completion(false)
                        }
                    } else {
                        completion(false)
                    }
                }
                catch let parsingError
                {
                    //JSON Parsing error
                    print(parsingError)
                    completion(false)
                }
            } else {
                completion(false)
            }
        }
    }
}
