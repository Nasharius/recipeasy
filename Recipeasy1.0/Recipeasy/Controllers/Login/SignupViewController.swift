//
//  LoginViewController.swift
//  Recipeasy
//
//  Created by James Hoffman on 2018-09-15.
//  Copyright © 2018 James Hoffman. All rights reserved.
//

import UIKit

class SignupViewController: UIViewController {
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = NSLocalizedString("Sign up", comment: "")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "signup.created-account") {
            if let recipesTableViewController = segue.destination as? RecipesTableViewController,
               let user = sender as? User {
                    recipesTableViewController.currentUser = user
            }
        }
    }
    
    @IBAction func onCreateAccountButtonTapped() {
        let username = usernameTextField.text ?? ""
        let password = passwordTextField.text ?? ""
        let passwordConfirmation = confirmPasswordTextField.text ?? ""
        
        if (username.isEmpty || password.isEmpty) {
            okAlert(title: NSLocalizedString("Error", comment: ""), message: NSLocalizedString("Password required", comment: ""))
        } else if (password != passwordConfirmation) {
            okAlert(title: NSLocalizedString("Error", comment: ""), message: NSLocalizedString("Password validation", comment: ""))
        }else {
            MarthaRequest.signUp(username: username, password: password) { (user) in
                DispatchQueue.main.async {
                    if let createdUser = user {
                        self.performSegue(withIdentifier: "signup.created-account", sender: createdUser)
                    } else {
                        self.okAlert(title: NSLocalizedString("Error", comment: ""), message: NSLocalizedString("Username taken", comment: ""))
                    }
                }
            }
        }
    }
}
