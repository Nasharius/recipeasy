//
//  LoginViewController.swift
//  Recipeasy
//
//  Created by James Hoffman on 2018-09-15.
//  Copyright © 2018 James Hoffman. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var rememberMeSwitch: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = NSLocalizedString("Recipeasy", comment: "")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        usernameTextField.text = SessionStorage.userDefaultsPassphrase
        passwordTextField.text = SessionStorage.keychainPassphrase
        
        if(passwordTextField.text != ""){
            onLoginButtonTapped()
        }
        self.rememberMeSwitch.isOn = false
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "login.logged-in") {
            if let recipesTableViewController = segue.destination as? RecipesTableViewController,
                let user = sender as? User {
                
                recipesTableViewController.currentUser = user
            }
        } else if (segue.identifier == "login.sign-up") {
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Cancel", comment: ""), style: .plain, target: nil, action: nil)
        }
    }
    
    @IBAction func onLoginButtonTapped() {
        let username = usernameTextField.text ?? ""
        let password = passwordTextField.text ?? ""
        
        if(rememberMeSwitch.isOn){
            SessionStorage.userDefaultsPassphrase = username
            SessionStorage.keychainPassphrase = password
        }
        MarthaRequest.login(username: username, password: password) { (user) in
            DispatchQueue.main.async {
                if let loggedUser = user {
                    self.performSegue(withIdentifier: "login.logged-in", sender: loggedUser)
                } else {
                    self.okAlert(title: NSLocalizedString("Error", comment: "") ,message: NSLocalizedString("Invalid login", comment: ""))
                }
            }
        }
    }
    
    @IBAction func unwindFromSignup(segue: UIStoryboardSegue) {
        if (segue.identifier == "signup.unwind-to-login") {
            if let signUpController = segue.source as? SignupViewController {
                usernameTextField.text = signUpController.usernameTextField.text
                passwordTextField.text = signUpController.passwordTextField.text
                
                onLoginButtonTapped()
            }
        }
    }
}
