DELETE FROM Recipes;
DELETE FROM Users;

INSERT INTO Users(id,username,pwd) VALUES (1,"mark",SHA2("1234", 256));
INSERT INTO Users(id,username,pwd) VALUES (2,"yolo",SHA2("1234", 256));

INSERT INTO Recipes(id,category,name,duration,description,user_id,image,created,modified) VALUES (1,2,"Poutine",5, "Fromages Frites Sauce BBQ",1,"",now(),now());
INSERT INTO Recipes(id,category,name,duration,description,user_id,image,created,modified) VALUES (2,2,"Hot Chicken",5, "Pain Mayonnaise Sauce BBQ Poulet Pois Carotte",1,"",now(),now());
INSERT INTO Recipes(id,category,name,duration,description,user_id,image,created,modified) VALUES (3,1,"Soupe Brocoli",5, "Eau Brocoli Fromage autre",1,"",now(),now());
INSERT INTO Recipes(id,category,name,duration,description,user_id,image,created,modified) VALUES (4,0,"Bagel BLT",5, "Laitue Tomate Fromage Bacon Bagel",1,"",now(),now());
INSERT INTO Recipes(id,category,name,duration,description,user_id,image,created,modified) VALUES (5,0,"Omelette",5, "Fromages Jambon Oeufs Piment",1,"",now(),now());

INSERT INTO Recipes(id,category,name,duration,description,user_id,image,created,modified) VALUES (6,2,"Vol au vent",5, "Pate Sauce Blanche Poulet Pois Carotte",2,"",now(),now());
INSERT INTO Recipes(id,category,name,duration,description,user_id,image,created,modified) VALUES (7,0,"Bacon",5, "Bacon",2,"",now(),now());
INSERT INTO Recipes(id,category,name,duration,description,user_id,image,created,modified) VALUES (8,2,"Spaghetti",5, "Nouille Sauce Viande Fromage",2,"",now(),now());
INSERT INTO Recipes(id,category,name,duration,description,user_id,image,created,modified) VALUES (9,2,"Pate Mexicain",5, "Piment Viande Pate Fromage",2,"",now(),now());
INSERT INTO Recipes(id,category,name,duration,description,user_id,image,created,modified) VALUES (10,1,"Jambon",5, "Jambon Whiskey",2,"",now(),now());